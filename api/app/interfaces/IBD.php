<?php
/**
 * File: IBD.php
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 25/10/16
 * Time: 00:01
 * Project: estudos-angular2
 * Copyright: 2016
 */
namespace app\interfaces;

interface IBD
{
		public static function listObject( $limit );

		public static function updateObject( $id, $attributes );

		public static function newObject( $attributes );

		public static function deleteObject( $id );

		public static function where( $campo, $valor, $tipo );
}