<?php
/**
 * File: toJson.php
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 25/10/16
 * Time: 00:06
 * Project: estudos-angular2
 * Copyright: 2016
 */
namespace app\classes;

class toJson
{
		private $dataArray = array();

		public function multipleJson( $returnedData )
		{
				foreach ( $returnedData as $data ):
						array_push( $this->dataArray, $data->to_array() );
				endforeach;

				return json_encode( $this->dataArray );
		}

		public function simpleJson( $returnedData )
		{
				return json_encode( $returnedData->to_array() );
		}

		public function toJson( $returnDados )
		{
				foreach ( $returnDados as $dado ):
						array_push( $this->dataArray, $dado->to_array() );
				endforeach;

				return json_encode( $this->dataArray );
		}
}