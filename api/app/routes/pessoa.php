<?php
/**
 * File: pessoa.php
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 24/10/16
 * Time: 23:55
 * Project: estudos-angular2
 * Copyright: 2016
 */
use app\models\Pessoa;
use app\classes\toJson;
use Slim\Http\Request;

/**
 * List Pessoas
 */
$app->get( '/pessoa', function () use ( $app ) {
		$pessoa = Pessoa::listObject();
		$toJson = new toJson();
		echo $jsonPessoa = $toJson->toJson( $pessoa );
} );
/**
 * Update pessoa
 */
$app->put( '/update-pessoa', function ( Request $request ) use ( $app ) {
		$dados      = $request->getParsedBody();
		$id         = filter_var( $dados[ 'id' ], FILTER_SANITIZE_NUMBER_INT );
		$nome       = filter_var( $dados[ 'tb_pessoa_nome' ], FILTER_SANITIZE_STRING );
		$email      = filter_var( $dados[ 'tb_pessoa_email' ], FILTER_SANITIZE_STRING );
		$tefixo     = filter_var( $dados[ 'tb_pessoa_telfixo' ], FILTER_SANITIZE_STRING );
		$estado     = filter_var( $dados[ 'tb_pessoa_estado' ], FILTER_SANITIZE_STRING );
		$endereco   = filter_var( $dados[ 'tb_pessoa_endereco' ], FILTER_SANITIZE_STRING );
		$attributes = [
			'tb_pessoa_nome'     => $nome,
			'tb_pessoa_email'    => $email,
			'tb_pessoa_telfixo'  => $tefixo,
			'tb_pessoa_estado'   => $estado,
			'tb_pessoa_endereco' => $endereco,
		];
		try {
				$pessoa = new Pessoa();
				$pessoa->updateObject( $id, $attributes );
				$result = array( "status" => "success",
				                 "data"   => "Pessoa atualizada com sucesso" );
				echo json_encode( $result );
		}
		catch ( \ActiveRecordException $e ) {
				$result = array( "status" => "error",
				                 "data"   => $e->getMessage() );
				echo json_encode( $result );
		}
} );
/**
 * Cadastro de Pessoa
 */
$app->post( '/add-pessoa', function ( Request $request ) use ( $app ) {
		$dados      = $request->getParsedBody();
		$nome       = filter_var( $dados[ 'tb_pessoa_nome' ], FILTER_SANITIZE_STRING );
		$email      = filter_var( $dados[ 'tb_pessoa_email' ], FILTER_SANITIZE_STRING );
		$tefixo     = filter_var( $dados[ 'tb_pessoa_telfixo' ], FILTER_SANITIZE_STRING );
		$estado     = filter_var( $dados[ 'tb_pessoa_estado' ], FILTER_SANITIZE_STRING );
		$endereco   = filter_var( $dados[ 'tb_pessoa_endereco' ], FILTER_SANITIZE_STRING );
		$attributes = [
			'tb_pessoa_nome'     => $nome,
			'tb_pessoa_email'    => $email,
			'tb_pessoa_telfixo'  => $tefixo,
			'tb_pessoa_estado'   => $estado,
			'tb_pessoa_endereco' => $endereco,
		];
		try {
				$pessoa = new Pessoa();
				$pessoa->newObject( $attributes );
				$result = array( "status" => "success",
				                 "data"   => "Pessoa cadastrada com sucesso" );
				echo json_encode( $result );
		}
		catch ( \ActiveRecordException $e ) {
				$result = array( "status" => "error",
				                 "data"   => $e->getMessage() );
				echo json_encode( $result );
		}
} );
/**
 * Deleta a pessoa
 */
$app->delete( '/delete-pessoa/{id}', function ( Request $request ) use ( $app ) {
		$id = filter_var( $request->getAttribute( 'id' ), FILTER_SANITIZE_NUMBER_INT );
		try {
				$pessso = new Pessoa();
				$pessso->deleteObject( $id );
				$result = array( "status" => "success",
				                 "data"   => "Pessoa deletada com sucesso" );
				echo json_encode( $result );
		}
		catch ( \ActiveRecordException $e ) {
				echo $e->getMessage();
		}
} );

 