<?php
/**
 * File: restaurante.php
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 24/10/16
 * Time: 23:55
 * Project: estudos-angular2
 * Copyright: 2016
 */
use app\models\Restaurante;
use app\classes\toJson;
use Slim\Http\Request;

/**
 * List restaurant
 */
$app->get( '/restaurante', function () use ( $app ) {
		$data   = restaurante::listObject();
		$toJson = new toJson();
		echo $jsonData = $toJson->toJson( $data );
} );
/**
 * Get restaurant by id
 */
$app->get( '/restaurante/{id}', function ( Request $request ) use ( $app ) {
		$id     = $request->getAttribute( 'id' );
		$data   = restaurante::listObjectById( $id );
		$toJson = new toJson();
		echo $jsonData = $toJson->simpleJson( $data );
} );
/**
 * Update restaurant
 */
$app->put( '/update-restaurante', function ( Request $request ) use ( $app ) {
		$dados      = json_decode($request->getBody());
		$id         = filter_var( $dados->id, FILTER_SANITIZE_NUMBER_INT );
		$nome       = filter_var( $dados->tb_restaurante_nome, FILTER_SANITIZE_STRING );
		$endereco   = filter_var( $dados->tb_restaurante_endereco, FILTER_SANITIZE_STRING );
		$descricao  = filter_var( $dados->tb_restaurante_descricao, FILTER_SANITIZE_STRING );
		$imagem     = filter_var( $dados->tb_restaurante_imagem, FILTER_SANITIZE_STRING );
		$preco      = filter_var( $dados->tb_restaurante_preco, FILTER_SANITIZE_STRING );
		$attributes = [
			'tb_restaurante_nome'      => $nome,
			'tb_restaurante_endereco'  => $endereco,
			'tb_restaurante_descricao' => $descricao,
			'tb_restaurante_imagem'    => $imagem,
			'tb_restaurante_preco'     => $preco,
		];

		try {
				$pessoa = new restaurante();
				$pessoa->updateObject( $id, $attributes );
				$result = array( "status" => "success",
				                 "data"   => "successfully updated record" );
				echo json_encode( $result );
		}
		catch ( \ActiveRecordException $e ) {
				$result = array( "status" => "error",
				                 "data"   => $e->getMessage() );
				echo json_encode( $result );
		}
} );
/**
 * Cad restaurant
 */
$app->post( '/add-restaurante', function ( Request $request ) use ( $app ) {
		$dados      = json_decode($request->getBody());
		$nome       = filter_var( $dados->tb_restaurante_nome, FILTER_SANITIZE_STRING );
		$endereco   = filter_var( $dados->tb_restaurante_endereco, FILTER_SANITIZE_STRING );
		$descricao  = filter_var( $dados->tb_restaurante_descricao, FILTER_SANITIZE_STRING );
		$imagem     = filter_var( $dados->tb_restaurante_imagem, FILTER_SANITIZE_STRING );
		$preco      = filter_var( $dados->tb_restaurante_preco, FILTER_SANITIZE_STRING );
		$attributes = [
			'tb_restaurante_nome'      => $nome,
			'tb_restaurante_endereco'  => $endereco,
			'tb_restaurante_descricao' => $descricao,
			'tb_restaurante_imagem'    => $imagem,
			'tb_restaurante_preco'     => $preco,
		];

		try {
				$pessoa = new restaurante();
				$pessoa->newObject( $attributes );
				$result = array( "status" => "success",
				                 "data"   => "successfully registered record" );
				echo json_encode( $result );
		}
		catch ( \ActiveRecordException $e ) {
				$result = array( "status" => "error",
				                 "data"   => $e->getMessage() );
				echo json_encode( $result );
		}
} );
/**
 * Delete restaurant
 */
$app->delete( '/delete-restaurante/{id}', function ( Request $request ) use ( $app ) {
		$id = filter_var( $request->getAttribute( 'id' ), FILTER_SANITIZE_NUMBER_INT );
		try {
				$pessso = new restaurante();
				$pessso->deleteObject( $id );
				$result = array( "status" => "success",
				                 "data"   => "Successfully deleted record" );
				echo json_encode( $result );
		}
		catch ( \ActiveRecordException $e ) {
				echo $e->getMessage();
		}
} );