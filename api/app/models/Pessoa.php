<?php
/**
 * File: Pessoa.php
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 25/10/16
 * Time: 00:04
 * Project: estudos-angular2
 * Copyright: 2016
 */
namespace app\models;

class Pessoa extends AppModel
{
		static $table_name = "tb_pessoa";
}