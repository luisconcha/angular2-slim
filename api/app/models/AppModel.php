<?php
/**
 * File: AppModel.php
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 25/10/16
 * Time: 00:00
 * Project: estudos-angular2
 * Copyright: 2016
 */
namespace app\models;

use ActiveRecord\ActiveRecordException;
use ActiveRecord\Model;
use app\interfaces\IBD;

class AppModel extends Model implements IBD
{
		public static function listObject( $limit = null )
		{
				if ( $limit != null ):
						return parent::find( 'all', array( 'select' => '*', 'limit' => $limit ) );
				endif;

				return parent::find( 'all' );
		}

		public static function listObjectById( $id )
		{
				return parent::find(  $id );
		}

		public static function updateObject( $id, $attributes )
		{
				$atualizar = parent::find( $id );
				$atualizar->update_attributes( $attributes );

				return $atualizar;
		}

		public static function newObject( $attributes )
		{
				return parent::create( $attributes );
		}

		public static function deleteObject( $id )
		{
				$deletar = parent::find( $id );
				$deletar->delete();
		}

		public static function where( $campo, $valor, $tipo )
		{
				// TODO: Implement where() method.
		}

}