<?php
/**
 * File: config.php
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 24/10/16
 * Time: 23:43
 * Project: estudos-angular2
 * Copyright: 2016
 */
session_start();
ini_set( "display_errors", 1 );
define( "ROOT", $_SERVER[ 'DOCUMENT_ROOT' ] . DIRECTORY_SEPARATOR );

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/connection.php';

$app = new \Slim\App( [
		'debug' => true,
] );