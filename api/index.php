<?php
/**
 * File: index.php
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 24/10/16
 * Time: 23:43
 * Project: estudos-angular2
 * Copyright: 2016
 */

require __DIR__ . '/config.php';
require __DIR__ . '/app/routes/pessoa.php';
require __DIR__ . '/app/routes/restaurante.php';


$app->run();
