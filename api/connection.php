<?php
/**
 * File: connection.php
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 24/10/16
 * Time: 23:44
 * Project: estudos-angular2
 * Copyright: 2016
 */
$db  = array(
	'database' => 'webapp',
	'hostname' => 'localhost',
	'user'     => 'root',
	'password' => '',
);
$cfg = ActiveRecord\Config::instance();
$cfg->set_model_directory( ROOT . 'app/models' );
$cfg->set_connections(
	array( 'development' => 'mysql://' . $db[ 'user' ] . $db[ 'password' ] . ':@' . $db[ 'hostname' ] . '/' . $db[ 'database' ] . '' )
);
 