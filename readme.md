##`Projeto em angular 2, Slim e active Record`

### AINDA ESTA EM DESENVOLVIMENTO !!!!

###Configurar a api

* Criar o virtualhost:
 ```sh   
    <VirtualHost *:80>
        ServerAdmin lacc-api.dev
        ServerName lacc-api.dev
        <IfModule mod_headers.c>
            Header set Access-Control-Allow-Origin "*"
        </IfModule>
        DocumentRoot /path_project/api
        <Directory "/path_project/api">
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>
 ```
* Criar o host:
 ```sh   
    127.0.0.1 lacc-api.dev
 ```
* Dentro da api (path_project/api)
 ```sh   
     composer install
  ```
* Verificar na url
  ```sh   
       http://lacc-api.dev/restaurante
  ```

###Configurar o front-end Angular  

* Dentro da raiz do projeto (path_project)
 ```sh   
     npm install
 ```  
 
* Rodar a aplicação (path_project)
 ```sh   
     npm star
 ```