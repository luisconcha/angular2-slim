/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 23/10/16
 * Time: 19:27
 * Project: estudos-angular2
 * Copyright: 2016
 */
import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RestaurantesListComponent} from "./component/restaurantes-list.component";
import {RestauranteEditComponent} from "./component/restaurante-edit.component";
import {RestauranteAddComponent} from "./component/restaurante-add.component";
import {PessoasListComponent} from "./component/pessoas-list.component";

const appRoutes: Routes = [
    {
        path:      '',
        component: RestaurantesListComponent,
        pathMatch: 'full'
    },
    {
        path:      'restaurante/:id/edit',
        component: RestauranteEditComponent
    },
    {
        path:      'restaurant/new',
        component: RestauranteAddComponent
    },
    {
        path:      'pessoas',
        component: PessoasListComponent
    }

];

export const routing: ModuleWithProviders = RouterModule.forRoot( appRoutes );