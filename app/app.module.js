/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 23/10/16
 * Time: 19:24
 * Project: estudos-angular2
 * Copyright: 2016
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require('@angular/http');
var app_component_1 = require("./app.component");
var forms_1 = require("@angular/forms");
var app_routing_1 = require('./app.routing');
var restaurantes_list_component_1 = require("./component/restaurantes-list.component");
var restaurante_service_1 = require("./services/restaurante.service");
var restaurante_edit_component_1 = require("./component/restaurante-edit.component");
var restaurante_add_component_1 = require("./component/restaurante-add.component");
var pessoas_list_component_1 = require("./component/pessoas-list.component");
var pessoa_service_1 = require("./services/pessoa.service");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, app_routing_1.routing, http_1.HttpModule],
            declarations: [app_component_1.AppComponent, restaurantes_list_component_1.RestaurantesListComponent, restaurante_edit_component_1.RestauranteEditComponent, restaurante_add_component_1.RestauranteAddComponent, pessoas_list_component_1.PessoasListComponent],
            bootstrap: [app_component_1.AppComponent],
            providers: [restaurante_service_1.RestauranteService, pessoa_service_1.PessoaService]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map