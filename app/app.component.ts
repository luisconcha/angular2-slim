/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 23/10/16
 * Time: 19:25
 * Project: estudos-angular2
 * Copyright: 2016
 */
import {Component} from '@angular/core';

declare var module: any;

@Component( {
    selector:    'my-app',
    templateUrl: './view/home.html',
    moduleId:    module.id
} )

export class AppComponent {
    public titulo:string =  "Api with Slim and angular 2";
}
