"use strict";
var router_1 = require('@angular/router');
var restaurantes_list_component_1 = require("./component/restaurantes-list.component");
var restaurante_edit_component_1 = require("./component/restaurante-edit.component");
var restaurante_add_component_1 = require("./component/restaurante-add.component");
var pessoas_list_component_1 = require("./component/pessoas-list.component");
var appRoutes = [
    {
        path: '',
        component: restaurantes_list_component_1.RestaurantesListComponent,
        pathMatch: 'full'
    },
    {
        path: 'restaurante/:id/edit',
        component: restaurante_edit_component_1.RestauranteEditComponent
    },
    {
        path: 'restaurant/new',
        component: restaurante_add_component_1.RestauranteAddComponent
    },
    {
        path: 'pessoas',
        component: pessoas_list_component_1.PessoasListComponent
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map