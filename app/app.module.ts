/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 23/10/16
 * Time: 19:24
 * Project: estudos-angular2
 * Copyright: 2016
 */

import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import { HttpModule } from '@angular/http';
import {AppComponent} from "./app.component";
import {FormsModule} from "@angular/forms";
import {routing} from './app.routing';
import {RestaurantesListComponent} from "./component/restaurantes-list.component";
import {RestauranteService} from "./services/restaurante.service";
import {RestauranteEditComponent} from "./component/restaurante-edit.component";
import {RestauranteAddComponent} from "./component/restaurante-add.component";
import {PessoasListComponent} from "./component/pessoas-list.component";
import {PessoaService} from "./services/pessoa.service";

@NgModule( {
    imports:      [ BrowserModule, FormsModule, routing,HttpModule ],
    declarations: [ AppComponent, RestaurantesListComponent, RestauranteEditComponent, RestauranteAddComponent, PessoasListComponent ],
    bootstrap:    [ AppComponent ],
    providers:    [ RestauranteService, PessoaService ]
} )

export class AppModule {

}