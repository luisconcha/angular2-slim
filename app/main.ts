/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 23/10/16
 * Time: 19:23
 * Project: estudos-angular2
 * Copyright: 2016
 */

import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {AppModule} from "./app.module";

const platform = platformBrowserDynamic();

platform.bootstrapModule( AppModule );