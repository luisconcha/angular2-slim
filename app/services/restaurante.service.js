"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 21/10/16
 * Time: 20:58
 * Project: estudos-angular2
 * Copyright: 2016
 */
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require('rxjs/add/operator/map');
var RestauranteService = (function () {
    function RestauranteService(_http) {
        this._http = _http;
        this.base_url = 'http://lacc-api.dev';
    }
    RestauranteService.prototype.getRestaurantes = function () {
        return this._http.get(this.base_url + '/restaurante').map(function (res) { return res.json(); });
    };
    RestauranteService.prototype.getRestauranteById = function (id) {
        return this._http.get(this.base_url + '/restaurante/' + id).map(function (res) { return res.json(); });
    };
    RestauranteService.prototype.editRestaurante = function (restaurante) {
        console.log('restaurante: ', restaurante);
        var json = JSON.stringify(restaurante);
        var params = "json=" + json;
        var headers = new http_1.Headers({ "Content-Type": "application/x-www-form-urlencoded" });
        return this._http.put(this.base_url + '/update-restaurante', JSON.stringify(restaurante), { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestauranteService.prototype.addRestaurante = function (restaurante) {
        var json = JSON.stringify(restaurante);
        var params = "json=" + json;
        var headers = new http_1.Headers({ "Content-Type": "application/x-www-form-urlencoded" });
        return this._http.post(this.base_url + '/add-restaurante', JSON.stringify(restaurante), { headers: headers }).map(function (res) { return res.json(); });
    };
    RestauranteService.prototype.deleteRestaurante = function (id) {
        var headers = new http_1.Headers({ "Content-Type": "application/x-www-form-urlencoded" });
        return this._http.delete(this.base_url + '/delete-restaurante/' + id, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RestauranteService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], RestauranteService);
    return RestauranteService;
}());
exports.RestauranteService = RestauranteService;
//# sourceMappingURL=restaurante.service.js.map