/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 21/10/16
 * Time: 20:58
 * Project: estudos-angular2
 * Copyright: 2016
 */
import {Injectable} from "@angular/core";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';
import {Restaurante} from "../model/restaurante";

@Injectable()

export class RestauranteService {

    private base_url = 'http://lacc-api.dev';
    public filesToUpload: Array<File>;
    public resultUpload;
    public restaurante: Restaurante;

    constructor( private _http: Http ) {
    }

    getRestaurantes() {
        return this._http.get( this.base_url + '/restaurante' ).map( res => res.json() );
    }

    getRestauranteById( id: number ) {
        return this._http.get( this.base_url + '/restaurante/' + id ).map( res => res.json() );
    }

    editRestaurante( restaurante: Restaurante ) {
        console.log('restaurante: ', restaurante);
        let json    = JSON.stringify( restaurante );
        let params  = "json=" + json;
        let headers = new Headers( { "Content-Type": "application/x-www-form-urlencoded" } );
        return this._http.put( this.base_url + '/update-restaurante', JSON.stringify( restaurante ), { headers: headers } )
            .map( res => res.json() );
    }

    addRestaurante( restaurante: Restaurante ) {
        let json    = JSON.stringify( restaurante );
        let params  = "json=" + json;
        let headers = new Headers( { "Content-Type": "application/x-www-form-urlencoded" } );
        return this._http.post( this.base_url + '/add-restaurante', JSON.stringify( restaurante ), { headers: headers } ).map( res=>res.json() );
    }


    deleteRestaurante( id: string ) {
        let headers = new Headers( { "Content-Type": "application/x-www-form-urlencoded" } );
        return this._http.delete( this.base_url + '/delete-restaurante/' + id, { headers: headers } )
            .map( res => res.json() );
    }

    // /**
    //  * Metodo encarregado de subir os arquivos ao servidor
    //  * @param url
    //  * @param params
    //  * @param files
    //  * @returns {Promise<T>|Promise}
    //  */
    // makeFileRequest( url: string, params: Array<string>, files: Array<File> ) {
    //     return new Promise( ( resolve, reject ) => {
    //         var formData: any = new FormData();
    //         var xhr           = new XMLHttpRequest();
    //
    //         for ( var i = 0; i < files.length; i++ ) {
    //             formData.append( "uploads[]", files[ i ], files[ i ].name );
    //         }
    //
    //         xhr.onreadystatechange = function () {
    //             if ( xhr.readyState == 4 ) {
    //                 if ( xhr.status == 200 ) {
    //                     resolve( JSON.parse( xhr.response ) );
    //                 } else {
    //                     reject( xhr.response );
    //                 }
    //             }
    //         };
    //         xhr.open( "POST", url, true );
    //         xhr.send( formData );
    //     } );
    // }
}
