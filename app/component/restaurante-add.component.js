"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 24/10/16
 * Time: 22:13
 * Project: estudos-angular2
 * Copyright: 2016
 */
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var restaurante_service_1 = require("../services/restaurante.service");
var restaurante_1 = require("../model/restaurante");
var RestauranteAddComponent = (function () {
    function RestauranteAddComponent(restauranteService, router) {
        this.restauranteService = restauranteService;
        this.router = router;
        this.restaurante = new restaurante_1.Restaurante();
        this.titulo = "Cadastrar restaurante";
    }
    RestauranteAddComponent.prototype.ngOnInit = function () {
        this.restaurante.tb_restaurante_imagem = null;
    };
    RestauranteAddComponent.prototype.onSubmit = function () {
        var _this = this;
        this.restauranteService.addRestaurante(this.restaurante).subscribe(function (response) {
            console.log('response: ', response);
            _this.status = response.status;
            if (_this.status !== 'success') {
                alert('error no servidor');
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage !== null) {
                alert('Houve um error na requisição');
            }
        });
        this.router.navigate(['/']);
    };
    RestauranteAddComponent.prototype.callNivelPreco = function (value) {
        this.restaurante.tb_restaurante_preco = value;
    };
    RestauranteAddComponent.prototype.cancelar = function () {
        this.router.navigate(['/']);
    };
    RestauranteAddComponent = __decorate([
        core_1.Component({
            selector: 'task-new',
            templateUrl: "../view/restaurante-add.html",
            moduleId: module.id
        }), 
        __metadata('design:paramtypes', [restaurante_service_1.RestauranteService, router_1.Router])
    ], RestauranteAddComponent);
    return RestauranteAddComponent;
}());
exports.RestauranteAddComponent = RestauranteAddComponent;
//# sourceMappingURL=restaurante-add.component.js.map