/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 23/10/16
 * Time: 19:29
 * Project: estudos-angular2
 * Copyright: 2016
 */
import {Component, OnInit, Inject} from '@angular/core';
import {Router} from '@angular/router';
import {Pessoa} from "../model/pessoa";
import {PessoaService} from "../services/pessoa.service";

declare var module: any;

@Component( {
    selector:    'pessoas-list',
    templateUrl: '../view/pessoas-list.html',
    moduleId:    module.id
} )

export class PessoasListComponent implements OnInit {
    public titulo: string  = "List people";
    public pessoas: Pessoa[];
    public status: string;
    public errorMessage: string;
    public loading;
    public confirmado;
    public base_url_imagem = 'http://lacc-api.dev/uploads/';
    public data_res;

    constructor( private _pessoaService: PessoaService, private router: Router ) {
    }

    ngOnInit(): void {
        this.loading = 'show';
        this.getPessoas();
    }

    getPessoas() {
        this._pessoaService.getPessoas().subscribe( result => {
            this.pessoas = result;
            console.log('Pessoas: ', result);
            this.loading = 'hide';
        }, error => {
            this.errorMessage = <any>error;
            if ( this.errorMessage !== null ) {
                console.log( 'Error: ', this.errorMessage );
                alert( 'Error na requisicion' );
            }
        } );
    }

    /**
     *
     * @param id
     */

    // goToEdit( id: number ) {
    //     this.router.navigate( [ 'restaurante', id, 'edit' ] );
    // }
    //
    // onBorrarConfirm( id ) {
    //     this.confirmado = id;
    // }
    //
    // onCancelarConfirm() {
    //     this.confirmado = null;
    // }
    //
    // onDeleteRestaurante(id){
    //     this._restauranteService.deleteRestaurante( id ).subscribe( result => {
    //         this.status = result.status;
    //         if ( this.status != 'success' ) {
    //             alert( 'Error en el servidor' );
    //         }
    //         this.getRestaurantes();
    //     }, error => {
    //         this.errorMessage = <any>error;
    //         if ( this.errorMessage !== null ) {
    //             alert( 'Error na requisicion' );
    //         }
    //         this.loading = 'hide';
    //     } );
    // }
}
