/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 23/10/16
 * Time: 19:29
 * Project: estudos-angular2
 * Copyright: 2016
 */
import {Component, OnInit, Inject} from '@angular/core';
import {Router} from '@angular/router';
import {RestauranteService} from "../services/restaurante.service";
import {Restaurante} from "../model/restaurante";

declare var module: any;

@Component( {
    selector:    'restaurantes-list',
    templateUrl: '../view/restaurantes-list.html',
    moduleId:    module.id
} )

export class RestaurantesListComponent implements OnInit {
    public titulo: string  = "List of restaurants";
    public restaurantes: Restaurante[];
    public status: string;
    public errorMessage: string;
    public loading;
    public confirmado;
    public base_url_imagem = 'http://lacc-api.dev/uploads/';
    public data_res;

    constructor( private _restauranteService: RestauranteService, private router: Router ) {
    }

    ngOnInit(): void {
        this.loading = 'show';
        this.getRestaurantes();
    }

    getRestaurantes() {
        this._restauranteService.getRestaurantes().subscribe( result => {
            this.restaurantes = result;
            this.loading = 'hide';
        }, error => {
            this.errorMessage = <any>error;
            if ( this.errorMessage !== null ) {
                console.log( 'Error: ', this.errorMessage );
                alert( 'Error na requisicion' );
            }
        } );
    }

    /**
     *
     * @param id
     */

    goToEdit( id: number ) {
        this.router.navigate( [ 'restaurante', id, 'edit' ] );
    }

    onBorrarConfirm( id ) {
        this.confirmado = id;
    }

    onCancelarConfirm() {
        this.confirmado = null;
    }

    onDeleteRestaurante(id){
        this._restauranteService.deleteRestaurante( id ).subscribe( result => {
            this.status = result.status;
            if ( this.status != 'success' ) {
                alert( 'Error en el servidor' );
            }
            this.getRestaurantes();
        }, error => {
            this.errorMessage = <any>error;
            if ( this.errorMessage !== null ) {
                alert( 'Error na requisicion' );
            }
            this.loading = 'hide';
        } );
    }
}
