/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 24/10/16
 * Time: 22:13
 * Project: estudos-angular2
 * Copyright: 2016
 */
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RestauranteService} from "../services/restaurante.service";
import {Restaurante} from "../model/restaurante";

declare var module: any;

@Component( {
    selector:    'task-new',
    templateUrl: "../view/restaurante-add.html",
    moduleId:    module.id
} )

export class RestauranteAddComponent implements OnInit {
    public restaurante    = new Restaurante();
    public status: string;
    public errorMessage: string;
    public titulo: string = "Cadastrar restaurante";

    constructor( private restauranteService: RestauranteService,
                 private router: Router ) {
    }

    ngOnInit() {
        this.restaurante.tb_restaurante_imagem = null;
    }

    onSubmit() {
        this.restauranteService.addRestaurante( this.restaurante ).subscribe(
            response => {
                console.log('response: ', response);
                this.status = response.status;
                if ( this.status !== 'success' ) {
                    alert( 'error no servidor' );
                }
            }, error => {
                this.errorMessage = <any>error;
                if ( this.errorMessage !== null ) {
                    alert( 'Houve um error na requisição' );
                }
            }
        );

        this.router.navigate( [ '/' ] );
    }

    callNivelPreco( value ) {
        this.restaurante.tb_restaurante_preco = value;
    }

    cancelar() {
        this.router.navigate( [ '/' ] );
    }

}