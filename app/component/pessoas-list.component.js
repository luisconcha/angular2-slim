"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 23/10/16
 * Time: 19:29
 * Project: estudos-angular2
 * Copyright: 2016
 */
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var pessoa_service_1 = require("../services/pessoa.service");
var PessoasListComponent = (function () {
    function PessoasListComponent(_pessoaService, router) {
        this._pessoaService = _pessoaService;
        this.router = router;
        this.titulo = "List people";
        this.base_url_imagem = 'http://lacc-api.dev/uploads/';
    }
    PessoasListComponent.prototype.ngOnInit = function () {
        this.loading = 'show';
        this.getPessoas();
    };
    PessoasListComponent.prototype.getPessoas = function () {
        var _this = this;
        this._pessoaService.getPessoas().subscribe(function (result) {
            _this.pessoas = result;
            console.log('Pessoas: ', result);
            _this.loading = 'hide';
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage !== null) {
                console.log('Error: ', _this.errorMessage);
                alert('Error na requisicion');
            }
        });
    };
    PessoasListComponent = __decorate([
        core_1.Component({
            selector: 'pessoas-list',
            templateUrl: '../view/pessoas-list.html',
            moduleId: module.id
        }), 
        __metadata('design:paramtypes', [pessoa_service_1.PessoaService, router_1.Router])
    ], PessoasListComponent);
    return PessoasListComponent;
}());
exports.PessoasListComponent = PessoasListComponent;
//# sourceMappingURL=pessoas-list.component.js.map