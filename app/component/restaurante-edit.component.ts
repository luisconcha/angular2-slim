/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 23/10/16
 * Time: 21:30
 * Project: estudos-angular2
 * Copyright: 2016
 */

import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Params, Router} from '@angular/router';
import {RestauranteService} from "../services/restaurante.service";
import {Restaurante} from "../model/restaurante";

declare var module: any;

@Component( {
    selector:    "restaurante-edit",
    templateUrl: "../view/restaurante-add.html",
    moduleId:    module.id
} )

export class RestauranteEditComponent implements OnInit {
    public restaurante    = new Restaurante();
    public status: string;
    public errorMessage: string;
    public titulo: string = "Editar restaurante";
    public loading;
    public filesToUpload: Array<File>;
    public resultUpload;
    private base_url      = 'http://api-restaurante.dev/restaurantes-api.php';
    public id;

    constructor( private _restauranteService: RestauranteService,
                 private route: ActivatedRoute,
                 private router: Router ) {
    }

    ngOnInit(): void {
        this.loading = 'show';
        this.route.params.forEach( ( params: Params ) => {
            this.id = +params[ 'id' ];
        } );
        this.getRestauranteById();
    }

    getRestauranteById() {
        this._restauranteService.getRestauranteById( this.id ).subscribe( result => {
            this.restaurante = result;
            this.loading = 'hide';
        }, error => {
            this.errorMessage = <any>error;
            if ( this.errorMessage !== null ) {
                alert( 'Houve um error na requisição' );
            }
        } );
    }

    cancelar() {
        this.router.navigate( [ '/' ] );
    }

    onSubmit(){
        this._restauranteService.editRestaurante(this.restaurante ).subscribe( result => {
            this.restaurante = result.data;
            this.loading = 'hide';
        }, error => {
            this.errorMessage = <any>error;
            if ( this.errorMessage !== null ) {
                alert( 'Houve um error na requisição' );
            }
        } );

        this.router.navigate( [ '/' ] );
    }


    callNivelPreco( value ) {
        this.restaurante.tb_restaurante_preco = value;
    }
}