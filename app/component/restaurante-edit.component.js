/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 23/10/16
 * Time: 21:30
 * Project: estudos-angular2
 * Copyright: 2016
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require('@angular/router');
var restaurante_service_1 = require("../services/restaurante.service");
var restaurante_1 = require("../model/restaurante");
var RestauranteEditComponent = (function () {
    function RestauranteEditComponent(_restauranteService, route, router) {
        this._restauranteService = _restauranteService;
        this.route = route;
        this.router = router;
        this.restaurante = new restaurante_1.Restaurante();
        this.titulo = "Editar restaurante";
        this.base_url = 'http://api-restaurante.dev/restaurantes-api.php';
    }
    RestauranteEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loading = 'show';
        this.route.params.forEach(function (params) {
            _this.id = +params['id'];
        });
        this.getRestauranteById();
    };
    RestauranteEditComponent.prototype.getRestauranteById = function () {
        var _this = this;
        this._restauranteService.getRestauranteById(this.id).subscribe(function (result) {
            _this.restaurante = result;
            _this.loading = 'hide';
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage !== null) {
                alert('Houve um error na requisição');
            }
        });
    };
    RestauranteEditComponent.prototype.cancelar = function () {
        this.router.navigate(['/']);
    };
    RestauranteEditComponent.prototype.onSubmit = function () {
        var _this = this;
        this._restauranteService.editRestaurante(this.restaurante).subscribe(function (result) {
            _this.restaurante = result.data;
            _this.loading = 'hide';
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage !== null) {
                alert('Houve um error na requisição');
            }
        });
        this.router.navigate(['/']);
    };
    RestauranteEditComponent.prototype.callNivelPreco = function (value) {
        this.restaurante.tb_restaurante_preco = value;
    };
    RestauranteEditComponent = __decorate([
        core_1.Component({
            selector: "restaurante-edit",
            templateUrl: "../view/restaurante-add.html",
            moduleId: module.id
        }), 
        __metadata('design:paramtypes', [restaurante_service_1.RestauranteService, router_1.ActivatedRoute, router_1.Router])
    ], RestauranteEditComponent);
    return RestauranteEditComponent;
}());
exports.RestauranteEditComponent = RestauranteEditComponent;
//# sourceMappingURL=restaurante-edit.component.js.map