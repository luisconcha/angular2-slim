/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 21/10/16
 * Time: 20:55
 * Project: estudos-angular2
 * Copyright: 2016
 */

export class Restaurante {
    // constructor( public id: number,
    //              public nombre: string,
    //              public direccion: string,
    //              public descripcion: string,
    //              public imagen: string,
    //              public precio: string ) {
    // }
    public id: number;
    public tb_restaurante_nome: string;
    public tb_restaurante_endereco: string;
    public tb_restaurante_descricao: string;
    public tb_restaurante_imagem: string;
    public tb_restaurante_preco: string;

}