/**
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: typeScript
 * Date: 21/10/16
 * Time: 20:55
 * Project: estudos-angular2
 * Copyright: 2016
 */

export class Pessoa {
    public id: number;
    public tb_pessoa_nome: string;
    public tb_pessoa_email: string;
    public tb_pessoa_telfixo: string;
    public tb_pessoa_estado: string;
    public tb_pessoa_endereco: string;

}